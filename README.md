# Purpose

This repository exists as an automated alternative to [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-create-a-kubernetes-1-10-cluster-using-kubeadm-on-ubuntu-16-04) on Digital Ocean (Or other PaaS's) for creating a **Kubernetes** cluster on **Ubuntu** **18.04** (and probably other versions like **16.04**) with **Ansible**.

# About

I came across this after following different tutorials and methodologies for easily bootstrapping a **Kubernetes** cluster. I'd spun up a **CoreOS** cluster following [this tutorial](https://typhoon.psdn.io/), an **Ubuntu** cluster using [this tutorial here](https://github.com/kubernetes-digitalocean-terraform/kubernetes-digitalocean-terraform). I wasn't really satisfied with either of them.

# License

* Author of the majority of the **Ansible** files: https://www.digitalocean.com/community/users/bsder
* Copyright 2018 DigitalOcean(TM) Inc.
* Original License: [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) ](https://creativecommons.org/licenses/by-nc-sa/4.0/)

# Usage

1. Create the servers to be used on Digital Ocean, with **Private Networking** enabled upon creation. This is because if you do not provide an IP address as shown below, it will default to communicate between nodes over the Public IP Address. Not ideal. So it assumes `eth1` is the correct interface for **Private Networking**.

```bash
kubeadm init --apiserver-advertise-address='{{ ansible_eth1.ipv4.address }}' --pod-network-cidr=10.244.0.0/16 >> cluster_initialized.txt
```
The good news is that is still listens on `:6443` from any interface:

```bash
ubuntu@ubuntu-s-1vcpu-1gb-sfo2-01:~$ sudo netstat -eplunta | grep LISTEN | grep ":6443"
tcp6 0 0 :::6443 :::* LISTEN 0 81887 24435/kube-apiserve
```

2. Install `ansible` on your system.
3. Update the `hosts` file with the IPs/DNS Names of these servers you created.
4. Execute each **Ansible** file manually like with `ansible-playbook -v --private-key -i hosts 1-setup.yml` (and so on), sequentially, or use `find` to augment your laziness like a boss:

```bash
find /path/to/repo/directory -name '*.yml' -print0 | sort -z | xargs -r0 ansible-playbook -i hosts
```

5. `ssh` into the master node.
6. Change user: `sudo su - kubernetes`
7. Ensure the cluster is working with: `kubectl get nodes`:

```
ubuntu@ubuntu-s-1vcpu-1gb-sfo2-01:~$ kubectl get nodes
NAME                         STATUS    ROLES     AGE       VERSION
ubuntu-s-1vcpu-1gb-sfo2-01   Ready     master    4m        v1.12.1
ubuntu-s-1vcpu-1gb-sfo2-02   Ready     <none>    4m        v1.12.1
```

## Important Notes

+ You should enable a deny-by-default firewall. See: https://github.com/freach/kubernetes-security-best-practice/blob/master/README.md#firewall-ports-fire https://kubernetes.io/docs/tasks/administer-cluster/securing-a-cluster/ and https://github.com/freach/kubernetes-security-best-practice/blob/master/README.md#firewall-ports-fire
